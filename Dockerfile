FROM node:16.16.0-alpine AS base
RUN npm i -g pnpm

FROM base AS dependencies

WORKDIR /app
COPY package.json pnpm-lock.yaml ./
RUN pnpm install

# generated prisma files
COPY prisma ./prisma/

# COPY ENV variable
COPY .env ./

# COPY tsconfig.json file
COPY tsconfig.json ./

# COPY
COPY . .

RUN pnpx prisma generate

# Run and expose the server on port 3000
EXPOSE 3000

# A command to start the server
CMD pnpm start