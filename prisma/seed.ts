import {PrismaClient, Prisma} from '@prisma/client';
import bcrypt from 'bcryptjs';

const prisma = new PrismaClient();

const userData: Prisma.UserCreateInput[] = [
  {
    name: 'Admin',
    email: 'admin@gmail.com',
    password: bcrypt.hashSync('test123', 8)
  },
  {
    name: 'User 2',
    email: 'user2@gmail.com',
    password: bcrypt.hashSync('hungry123', 8)
  }
];

const menuData: Prisma.MenuCreateInput[] = [
  {
    name: 'Chicken Wing',
    price: 40000,
    quantity: 20,
    assetUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Iksan_City_48_Korean_Style_Fried_chicken.jpg/420px-Iksan_City_48_Korean_Style_Fried_chicken.jpg'
  },
  {
    name: 'Kopi Susu Aren',
    price: 12000,
    quantity: 10,
    assetUrl:
      'https://coffeeland.co.id/wp-content/uploads/2019/11/kopi-susu-534x800.jpg'
  },
  {
    name: 'Nasi Putih',
    price: 5000,
    quantity: 5,
    assetUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Nasi_dibentuk_bulat.jpg/800px-Nasi_dibentuk_bulat.jpg'
  }
];

async function main() {
  console.log(`Start seeding ...`);
  for (const u of userData) {
    const user = await prisma.user.create({
      data: u
    });
    console.log(`Created user with id: ${user.id}`);
  }
  for (const menu of menuData) {
    const menuItem = await prisma.menu.create({
      data: menu
    });
    console.log(`Created menu with id: ${menuItem.id}`);
  }
  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async e => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
