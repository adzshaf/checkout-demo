import jwt from 'jsonwebtoken';
import createError from 'http-errors';
import {User} from '@prisma/client';

const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET as string;

const signAccessToken = (payload: User) => {
  return new Promise((resolve, reject) => {
    jwt.sign({payload}, accessTokenSecret, {}, (err, token) => {
      if (err) {
        reject(createError.InternalServerError());
      }
      resolve(token);
    });
  });
};

const verifyAccessToken = (token: string) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, accessTokenSecret, (err, payload) => {
      if (err) {
        const message =
          err.name == 'JsonWebTokenError' ? 'Unauthorized' : err.message;
        return reject(createError.Unauthorized(message));
      }
      resolve(payload);
    });
  });
};

export {signAccessToken, verifyAccessToken};
