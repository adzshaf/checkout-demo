import {Prisma, PrismaClient, User} from '@prisma/client';
import {NextFunction, Request, Response} from 'express';
import createError from 'http-errors';
import {verifyAccessToken} from '../util/jwt';

const prisma = new PrismaClient();

const auth = async (req: Request, res: Response, next: NextFunction) => {
  if (!req.headers.authorization) {
    return res
      .status(401)
      .json(createError.Unauthorized('Access token is required'));
  }
  const token = req.headers.authorization.split(' ')[1];
  if (!token) {
    return res.status(401).json(createError.Unauthorized());
  }
  await verifyAccessToken(token)
    .then(async value => {
      let user = value as {payload: User; lat: number};

      let userValue = await prisma.user.findUnique({
        where: {
          email: user.payload.email
        }
      });

      if (!userValue) {
        throw createError.NotFound('User not registered');
      }

      req.user = {
        id: user.payload.id,
        email: user.payload.email
      };
      next();
    })
    .catch(e => {
      return res.status(401).json(createError.Unauthorized(e.message));
    });
};

export {auth};
