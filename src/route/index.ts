import express from 'express';
import {AuthController} from '../controller/auth.controller';
import {CartController} from '../controller/cart.controller';
import {MenuController} from '../controller/menu.controller';
import {OrderController} from '../controller/order.controller';
import {auth} from '../middleware/auth';

const router = express.Router();

router.post(`/signup`, AuthController.signup);
router.post(`/login`, AuthController.login);

router.get('/menu', auth, MenuController.getAllMenus);

router.post('/cart', auth, CartController.addCartItem);
router.put('/cart', auth, CartController.updateCartItem);
router.delete('/cart', auth, CartController.deleteCartItem);
router.get('/cart', auth, CartController.getCart);

router.post('/checkout', auth, OrderController.checkout);
router.get('/order/:id', auth, OrderController.getOne);

export default router;
