import {Request, Response} from 'express';
import {CartItemService, CartService} from '../service/cart.service';
import createError from 'http-errors';
import {ZodError, z} from 'zod';
import {fromZodError} from 'zod-validation-error';
import {Cart} from '@prisma/client';

class CartController {
  static async addCartItem(req: Request, res: Response) {
    try {
      let id = req.user?.id as number;
      let cart = await CartService.getOne(id);

      if (!cart) {
        cart = await CartService.create(id);
      }

      z.object({
        menuId: z.number(),
        cartId: z.number(),
        quantity: z.number()
      }).parse({
        menuId: req.body.menuId,
        cartId: cart?.id,
        quantity: req.body.quantity
      });

      let cartItem = await CartItemService.create({
        menuId: req.body.menuId,
        cartId: cart?.id as number,
        quantity: req.body.quantity
      });

      return res.status(200).json({
        status: true,
        message: 'Create cart item successful',
        data: cartItem
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof ZodError) {
        return res.status(400).json(fromZodError(error));
      } else if (error instanceof createError[400]) {
        return res.status(400).json(createError(error.message));
      } else if (error instanceof createError[404]) {
        return res.status(404).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }

  static async updateCartItem(req: Request, res: Response) {
    try {
      z.object({
        id: z.number(),
        quantity: z.number()
      }).parse({
        id: req.body.id,
        quantity: req.body.quantity
      });

      let cartItem = await CartItemService.update({
        id: req.body.id,
        quantity: req.body.quantity,
        userId: req.user?.id as number
      });

      return res.status(200).json({
        status: true,
        message: 'Update cart item successful',
        data: cartItem
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof ZodError) {
        return res.status(400).json(fromZodError(error));
      } else if (error instanceof createError[400]) {
        return res.status(400).json(createError(error.message));
      } else if (error instanceof createError[403]) {
        return res.status(403).json(createError(error.message));
      } else if (error instanceof createError[404]) {
        return res.status(404).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }

  static async deleteCartItem(req: Request, res: Response) {
    try {
      z.object({
        id: z.number()
      }).parse({
        id: req.body.id
      });

      let cartItem = await CartItemService.delete({
        id: req.body.id,
        userId: req.user?.id as number
      });

      return res.status(200).json({
        status: true,
        message: 'Delete cart item successful',
        data: cartItem
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof ZodError) {
        return res.status(400).json(fromZodError(error));
      } else if (error instanceof createError[404]) {
        return res.status(404).json(createError(error.message));
      } else if (error instanceof createError[403]) {
        return res.status(403).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }

  static async getCart(req: Request, res: Response) {
    let cart = await CartService.getOne(req.user?.id as number);

    return res.status(200).json({
      status: true,
      message: 'Get cart successful',
      data: cart
    });
  }
}

export {CartController};
