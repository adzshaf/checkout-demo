import {Request, Response} from 'express';
import {MenuService} from '../service/menu.service';

class MenuController {
  static async getAllMenus(_: Request, res: Response) {
    const menus = await MenuService.getAll();
    return res.status(200).json({
      status: true,
      message: 'Get all menu successful',
      data: menus
    });
  }
}

export {MenuController};
