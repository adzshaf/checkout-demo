import {AuthService} from '../service/auth.service';
import createError from 'http-errors';
import {NextFunction, Request, Response} from 'express';
import {z, ZodError} from 'zod';
import {fromZodError} from 'zod-validation-error';

class AuthController {
  static async signup(req: Request, res: Response, _: NextFunction) {
    try {
      z.object({
        email: z.string().email(),
        password: z.string(),
        name: z.string().optional()
      }).parse({
        email: req.body.email,
        password: req.body.password,
        name: req.body.name
      });

      const user = await AuthService.register(req.body);
      return res.status(200).json({
        status: true,
        message: 'User created successfully',
        data: user
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof ZodError) {
        return res.status(400).json(fromZodError(error));
      } else if (error instanceof createError[409]) {
        return res.status(409).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }

  static async login(req: Request, res: Response, _: NextFunction) {
    try {
      z.object({
        email: z.string().email(),
        password: z.string()
      }).parse({email: req.body.email, password: req.body.password});

      const user = await AuthService.login(req.body);
      return res.status(200).json({
        status: true,
        message: 'Account login successful',
        data: user
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof ZodError) {
        return res.status(400).json(fromZodError(error));
      } else if (error instanceof createError[401]) {
        return res.status(401).json(createError(error.message));
      } else if (error instanceof createError[404]) {
        return res.status(404).json(createError(error.message));
      } else if (error instanceof createError[409]) {
        return res.status(409).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }
}

export {AuthController};
