import {Request, Response} from 'express';
import {OrderService} from '../service/order.service';
import createError from 'http-errors';

class OrderController {
  static async checkout(req: Request, res: Response) {
    try {
      let order = await OrderService.create(req.user?.id as number);

      return res.status(200).json({
        status: true,
        message: 'Checkout successful',
        data: order
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof createError[400]) {
        return res.status(400).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }

  static async getOne(req: Request, res: Response) {
    try {
      let order = await OrderService.getOne({
        userId: req.user?.id as number,
        id: Number(req.params.id)
      });

      return res.status(200).json({
        status: true,
        message: 'Get order successful',
        data: order
      });
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof createError[404]) {
        return res.status(404).json(createError(error.message));
      } else if (error instanceof createError[403]) {
        return res.status(403).json(createError(error.message));
      } else if (error instanceof Error) {
        message = error.message;
      }
      return res.status(500).json(createError(message));
    }
  }
}

export {OrderController};
