import {PrismaClient} from '@prisma/client';
import {CartItemService, CartService} from './cart.service';
import {MenuService} from './menu.service';
import createError from 'http-errors';

const prisma = new PrismaClient();

class OrderService {
  static async create(userId: number) {
    try {
      let cart = await CartService.getOne(userId);
      let price = 0;

      cart?.cartItems.map(value => {
        if (value.quantity > value.menu.quantity) {
          throw createError.BadRequest(
            `Quantity ordered of ${value.quantity} cannot be bigger than existing stock of ${value.menu.quantity} for ${value.menu.name} `
          );
        }

        price += value.quantity * value.menu.price;
      });

      let orderItems = cart?.cartItems.map(item => {
        return {menuId: item.menuId, quantity: item.quantity};
      });

      let order = await prisma.orders.create({
        data: {
          userId: userId,
          price: price,
          orderItems: {
            create: orderItems
          }
        }
      });

      await CartItemService.deleteMany(cart?.id as number);
      await MenuService.updateAll(order.id);

      return order;
    } catch (error) {
      if (error instanceof Error) {
        throw createError.BadRequest(error.message);
      }
    }
  }

  static async getOne(input: {userId: number; id: number}) {
    let order = await prisma.orders.findFirst({
      where: {
        id: input.id
      },
      select: {
        userId: true,
        price: true,
        orderItems: {
          select: {
            menuId: true,
            menu: {
              select: {
                name: true,
                assetUrl: true,
                price: true
              }
            },
            quantity: true
          }
        }
      }
    });

    if (!order) {
      throw createError.NotFound('Order does not exist');
    }

    if (order?.userId !== input.userId) {
      throw createError.Forbidden("Cannot get other user's order");
    }

    return order;
  }
}

export {OrderService};
