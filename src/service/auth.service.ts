import bcrypt from 'bcryptjs';
import {PrismaClient, User, Prisma} from '@prisma/client';
import {signAccessToken} from '../util/jwt';
import createError from 'http-errors';

const prisma = new PrismaClient();

class AuthService {
  static async register(data: User) {
    data.password = bcrypt.hashSync(data.password as string, 8);

    try {
      let user = await prisma.user.create({
        data
      });
      let accessToken = await signAccessToken(user);

      return {email: user.email, name: user.name, accessToken};
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw createError.Conflict(
            'There is a unique constraint violation, a new user cannot be created with this email'
          );
        }
      }
    }
  }

  static async login(data: {email: string; password: string}) {
    const {email, password} = data;
    const user = await prisma.user.findUnique({
      where: {
        email
      }
    });

    if (!user) {
      throw createError.NotFound('User not registered');
    }

    const checkPassword = bcrypt.compareSync(password, user.password as string);
    if (!checkPassword)
      throw createError.Unauthorized('Email address or password not valid');

    const accessToken = await signAccessToken(user);
    return {email: user.email, name: user.name, accessToken};
  }
}

export {AuthService};
