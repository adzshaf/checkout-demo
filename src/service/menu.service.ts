import {PrismaClient} from '@prisma/client';

const prisma = new PrismaClient();

class MenuService {
  static async getAll() {
    let menus = await prisma.menu.findMany();

    return menus;
  }

  static async update(input: {menuId: number; newValue: number}) {
    let menu = await prisma.menu.update({
      where: {
        id: input.menuId
      },
      data: {
        quantity: input.newValue
      }
    });

    return menu;
  }

  static async updateAll(orderId: number) {
    let menus =
      await prisma.$executeRaw`UPDATE Menu INNER JOIN OrderItem on Menu.id = OrderItem.menuId INNER JOIN Orders on OrderItem.orderId = Orders.id SET Menu.quantity = Menu.quantity - OrderItem.quantity WHERE Orders.id = ${orderId}`;

    return menus;
  }
}

export {MenuService};
