import {Prisma, PrismaClient} from '@prisma/client';
import createError from 'http-errors';

const prisma = new PrismaClient();

class CartService {
  static async create(userId: number) {
    let cart = await prisma.cart.create({
      data: {
        userId: userId
      },
      select: {
        id: true,
        userId: true,
        user: {
          select: {
            name: true,
            email: true
          }
        },
        cartItems: {
          select: {
            quantity: true,
            menuId: true,
            menu: {
              select: {
                assetUrl: true,
                price: true,
                name: true,
                quantity: true
              }
            }
          }
        }
      }
    });

    return cart;
  }

  static async getOne(userId: number) {
    let cart = await prisma.cart.findUnique({
      where: {
        userId: userId
      },
      select: {
        id: true,
        userId: true,
        user: {
          select: {
            name: true,
            email: true
          }
        },
        cartItems: {
          select: {
            quantity: true,
            menuId: true,
            menu: {
              select: {
                assetUrl: true,
                price: true,
                name: true,
                quantity: true
              }
            }
          }
        }
      }
    });

    return cart;
  }
}

class CartItemService {
  static async create(input: {
    menuId: number;
    cartId: number;
    quantity: number;
  }) {
    try {
      let menu = await prisma.menu.findUnique({
        where: {
          id: input.menuId
        }
      });

      if (!menu) {
        throw createError.NotFound('Menu does not exist');
      }

      if ((menu?.quantity as number) < input.quantity) {
        throw createError.BadRequest(
          'Quantity cannot be bigger than existing stock'
        );
      }

      let cartItem = await prisma.cartItem.create({
        data: {
          menuId: input.menuId,
          cartId: input.cartId,
          quantity: input.quantity
        },
        select: {
          id: true,
          cartId: true,
          quantity: true,
          menuId: true,
          menu: {
            select: {
              assetUrl: true,
              price: true,
              name: true
            }
          }
        }
      });

      return cartItem;
    } catch (error) {
      throw error;
    }
  }

  static async update(input: {id: number; quantity: number; userId: number}) {
    try {
      let cartItem = await prisma.cartItem.findUnique({
        where: {
          id: input.id
        },
        select: {
          menu: true,
          cart: true
        }
      });

      if (!cartItem) {
        throw createError.NotFound('Cart item does not exist');
      }

      if ((cartItem.menu?.quantity as number) < input.quantity) {
        throw createError.BadRequest(
          'Quantity cannot be bigger than existing stock'
        );
      }

      if (cartItem.cart?.userId !== input.userId) {
        throw createError.Forbidden("Cannot update other user's cart item");
      }

      let updateCartItem = await prisma.cartItem.update({
        where: {
          id: input.id
        },
        data: {
          quantity: input.quantity
        },
        select: {
          id: true,
          cartId: true,
          quantity: true,
          menu: true
        }
      });

      return updateCartItem;
    } catch (error) {
      throw error;
    }
  }

  static async delete(input: {id: number; userId: number}) {
    try {
      let cartItem = await prisma.cartItem.findUnique({
        where: {
          id: input.id
        },
        select: {
          cart: true
        }
      });

      if (!cartItem) {
        throw createError.NotFound('Cart item does not exist');
      }

      if (cartItem.cart?.userId !== input.userId) {
        throw createError.Forbidden("Cannot update other user's cart item");
      }

      let deleteCartItem = await prisma.cartItem.delete({
        where: {
          id: input.id
        }
      });

      return deleteCartItem;
    } catch (error) {
      throw error;
    }
  }

  static async deleteMany(cartId: number) {
    let deleteCartItems = await prisma.cartItem.deleteMany({
      where: {
        cartId: cartId
      }
    });

    return deleteCartItems;
  }
}

export {CartService, CartItemService};
