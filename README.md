# Checkout Menu with Cart REST API

This repository tries to implement
a RESTful API which will provides these functions:

- Show menus
- Add item to the cart
- Update existing item in the cart
- Remove the item from the cart
- Checkout the cart and create an order

Note that these functions only can be accessed if the user is authenticated. It means additionally this code implements `/signin` and `/login` function as well.

This example uses Express with TypeScript, and MySQL using ORM of [Prisma Client](https://www.prisma.io/docs/concepts/components/prisma-client).

You can see initial data in [./prisma/seed.ts](./prisma/seed.ts). In the beginning, we has two users and three menus to try.

## Project Structure

These implementation codes are inside `src` folder. These are important folders to look out:

- `service` -> logic functions that will query data
- `controller` -> parsing request and response based on value from service
- `route` -> defining routes that are going to use

## Database Schema

See database schema based on [Prisma Schema](./prisma/schema.prisma). By running the command listed below, SQL script to create table will be automatically generated.

## Getting started

In order to run this code, you can try two steps, using Docker or manually.

<details><summary><strong>Manual</strong></summary>

You need to install NodeJS (>= v16) and MySQL already configured. You can use any package manager like npm, yarn. In this example. we use pnpm.

### 1. Clone this repository

### 2. Add `.env` file in root project

Add `.env` file in the root project with value of ACCESS_TOKEN_SECRET and DATABASE_URL. For example:

```
ACCESS_TOKEN_SECRET=checkout123
DATABASE_URL="mysql://root:test123*@localhost:3306/checkout"
```

### 3. Install dependencies

Install dependency using:

```bash
pnpm install
```

### 4. Run migration of Prisma

Run the following command to create your MySQL database file. This also creates tables that are defined in [`prisma/schema.prisma`](./prisma/schema.prisma):

Run this command:

```bash
pnpm prisma migrate dev --name <MIGRATION_NAME>
```

or

```
npx prisma migrate dev --name <MIGRATION_NAME>
```

When `npx prisma migrate dev` is executed against a newly created database, seeding is also triggered. The seed file in [`prisma/seed.ts`](./prisma/seed.ts) will be executed and your database will be populated with the sample data.

### 5. Run start server

Run command to start server:

```bash
pnpm start
```

### 4. Access `http://localhost:3000/`

</details>

<details><summary><strong>With Docker</strong></summary>

You need to install Docker, Docker Compose to run this code.

#### 1. Clone this repository

#### 2. Add `.env` file in root project

Add `.env` file in the root project with value of ACCESS_TOKEN_SECRET and DATABASE_URL. For example:

```
ACCESS_TOKEN_SECRET=checkout123
DATABASE_URL="mysql://root:test123*@db:3306/checkout"
```

Note that `DATABASE_URL` value is based on the values that are set on `docker-compose.yml`.

#### 3. Run docker compose

Run this command:

```bash
docker compose -f ./docker-compose.yml up
```

#### 4. Run migrate inside docker container

Run this command inside docker container of API. In this case, I use Docker Desktop so it is much easier:

```bash
pnpm prisma migrate dev --name init
```

#### 5. Access `http://localhost:3000/`

</details>

## Using the REST API

You can access the REST API of the server using the following endpoints:

### `POST` `/signup`

Here is the example of sign up request:

```json
{
  "email": "shafiya@gmail.com",
  "password": "shafiya123",
  "name": Shafiya
}
```

### `POST` `/login`

Here is the example of login request:

```json
{
  "email": "shafiya@gmail.com",
  "password": "shafiya123"
}
```

Login will return response of access token. Use the access token in order to access other endpoints. Add for example: `Bearer {access_token}` in header request (if you are using Postman or other API client, you can use Authorization section).

### `GET` `/menu`

This endpoint will show all menu with details of price, name, and quantity.

### `POST` `/cart` for add cart item.

Here is the example of add cart item to cart:

```json
{
  "menuId": 3,
  "quantity": 20
}
```

This request will add menu with ID of 4 and with quantity of 20 pieces to your cart. Use `menuId` and `quantity` based on property that we already get from listing of all menu.

It will return:

```json
{
  "status": true,
  "message": "Create cart item successful",
  "data": {
    "id": 10,
    "cartId": 1,
    "quantity": 20,
    "menuId": 4,
    "menu": {
      "assetUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Iksan_City_48_Korean_Style_Fried_chicken.jpg/420px-Iksan_City_48_Korean_Style_Fried_chicken.jpg",
      "price": 40000,
      "name": "Chicken Wing"
    }
  }
}
```

If you want to edit or delete cart item, you need to use ID (`id`) property.

### `PUT` `/cart` for update cart item.

Here is the example of update cart item to cart:

```json
{
  "id": 6,
  "quantity": 20
}
```

This request will update cart item with ID of 6 and with quantity of 20 pieces to your cart. Use `id` based on property that we already get from add cart item. Only quantity of item that can be modified for this case.

It will return:

```json
{
  "status": true,
  "message": "Update cart item successful",
  "data": {
    "id": 6,
    "cartId": 1,
    "quantity": 20,
    "menu": {
      "id": 4,
      "name": "Chicken Wing",
      "price": 40000,
      "assetUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Iksan_City_48_Korean_Style_Fried_chicken.jpg/420px-Iksan_City_48_Korean_Style_Fried_chicken.jpg",
      "quantity": 20
    }
  }
}
```

### `DELETE` `/cart` for delete cart item.

Here is the example of delete cart item from cart:

```json
{
  "id": 6
}
```

This request will update cart item with ID of 6. Use `id` based on property that we already get from add cart item.

### `POST` `/checkout` for checkout and add order.

This endpoint does not have request body. It automatically detects which items that already in your cart so it can be added to order.

It will return:

```json
{
  "status": true,
  "message": "Checkout successful",
  "data": {
    "id": 1,
    "userId": 4,
    "price": 800000
  }
}
```

Additional endpoints that are not included in requirements but may be necessary:

### `GET` `/cart` for getting information of cart from user

It will return:

```json
{
  "status": true,
  "message": "Get cart successful",
  "data": {
    "id": 1,
    "userId": 3,
    "user": {
      "name": "Admin",
      "email": "admin@gmail.com"
    },
    "cartItems": [
      {
        "quantity": 20,
        "menuId": 4,
        "menu": {
          "assetUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Iksan_City_48_Korean_Style_Fried_chicken.jpg/420px-Iksan_City_48_Korean_Style_Fried_chicken.jpg",
          "price": 40000,
          "name": "Chicken Wing",
          "quantity": 20
        }
      }
    ]
  }
}
```

### `GET` `/order/:id` for getting information of order from specific ID

Use ID for request params. The ID can be checked based on result of `POST /checkout`. It will return:

```json
{
  "status": true,
  "message": "Get order successful",
  "data": {
    "userId": 1,
    "price": 5000,
    "orderItems": [
      {
        "menuId": 3,
        "menu": {
          "name": "Nasi Putih",
          "assetUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Nasi_dibentuk_bulat.jpg/800px-Nasi_dibentuk_bulat.jpg",
          "price": 5000
        },
        "quantity": 1
      }
    ]
  }
}
```
